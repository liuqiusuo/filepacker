﻿using System;
using System.IO;
using System.Collections.Generic;

namespace FilePackerCmd
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string mainPath = "/Users/admin/Projects/SampleFiles";
			List<string> FilePathList = GetTestFilePathList( mainPath );

			Console.WriteLine ("Files Num: {0}", FilePathList.Count);

			DateTime start1 = DateTime.Now;
			TestSystemReadFiles (FilePathList);
			DateTime stop1 = DateTime.Now;

			Console.WriteLine ("System Timespend:{0}", stop1-start1);

			DateTime start2 = DateTime.Now;
			TestMyReadFiles (FilePathList);
			DateTime stop2 = DateTime.Now;

			Console.WriteLine ("System Timespend:{0}", stop2-start2);

		}

		public static void TestMyReadFiles(List<string> FilePathList)
		{
			//TestSystemReadFiles (FilePathList);
			foreach (var filePath in FilePathList) 
			{
				//PackedFile.ReadAllText(filePath);
				PackedFile pf = new PackedFile();
				byte[] bytes = pf.ReadAllBytes(filePath);
			}
		}

		public static void TestSystemReadFiles(List<string> FilePathList)
		{
			foreach (var filePath in FilePathList) 
			{
				//string text = File.ReadAllText(filePath);
				byte[] bytes = File.ReadAllBytes (filePath);
			}


//			DirectoryInfo folder = new DirectoryInfo("/Users/admin/Projects/SampleFiles");
//
//			foreach (FileInfo file in folder.GetFiles("*.*"))
//			{
//				Console.WriteLine(file.FullName);
//			}
		}



		public static List<string> GetTestFilePathList(string mainPath)
		{
			List<DirectoryInfo> DirInfoList = new List<DirectoryInfo>();

			DirInfoList.Add(new DirectoryInfo(mainPath));
			int DirInfoListIndex = 0;


			while (DirInfoListIndex < DirInfoList.Count)
			{
				DirectoryInfo[] dirs = new DirectoryInfo[] { };
				try
				{
					dirs = DirInfoList[DirInfoListIndex].GetDirectories();

				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}

				DirInfoList.AddRange(dirs);
				DirInfoListIndex++;
			}


			List<String> FilePathList = new List<string>();

			foreach (var dirInfo in DirInfoList)
			{
				FileInfo[] tFileInfos = new FileInfo[] { };

				try
				{
					tFileInfos = dirInfo.GetFiles();

				}
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}


				foreach (var fileInfo in tFileInfos)
				{
					string subFilePath = fileInfo.FullName;
					FilePathList.Add(subFilePath);
				}
			}

			return FilePathList;
		}
	}
}

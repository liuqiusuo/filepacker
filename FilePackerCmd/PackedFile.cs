﻿using System;
using System.IO;
using System.Collections.Generic;
using FilePacker;

namespace FilePackerCmd
{
	public class PackedFile
	{
		static PackedFile packedFile;
		static Dictionary<string, PackageHead.FileBlockInfo>  PackedFileDic = new Dictionary<string, PackageHead.FileBlockInfo>(); 
		static Dictionary<string, FileStream> PckFileStreamDic = new Dictionary<string, FileStream>();

		public PackedFile ()
		{
			if (packedFile == null) 
			{
				packedFile = this;
				ParsePckFiles ();
			}


		}

		public void ParsePckFiles()
		{
			
		}

		public string ReadAllText(string path)
		{
			return File.ReadAllText (path);
		}

		public byte[] ReadAllBytes(string path)
		{
			return File.ReadAllBytes (path);
		}
	}
}


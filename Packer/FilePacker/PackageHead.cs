﻿using System;
using System.Collections.Generic;

namespace FilePacker
{
	[Serializable]
	public class PackageHead
	{
		[Serializable]
		public class FileBlockInfo
		{
			public long offset;
			public long size;
		}

		public Dictionary<string,FileBlockInfo> FileInfosDic = new Dictionary<string, FileBlockInfo>();

		public PackageHead ()
		{
		}

	}
}


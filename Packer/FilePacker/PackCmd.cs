﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;

namespace FilePacker
{
	public class PackCmd
	{
		const string postfix = "pck";
		public PackCmd ()
		{
		}

		public static void Pack(string des, string rsc)
		{
			if (!des.EndsWith ("." + postfix)) 
			{
				des = des + "." + postfix;
			}
			if (File.Exists (des)) 
			{
				File.Delete (des);
			}
			using (FileStream fout = new FileStream(des, FileMode.Create, FileAccess.Write))
			{
				List<FileInfo> fileInfoList = GetFileInfoList (rsc);

				MemoryStream bodyStream = new MemoryStream ();
				PackageHead objHeadData = new PackageHead ();

				long offset = 0;


				foreach(var fileInfo in fileInfoList)
				{
					string path = fileInfo.FullName;

					PackageHead.FileBlockInfo fileBlockInfo = new PackageHead.FileBlockInfo ();
					fileBlockInfo.offset = offset;
					fileBlockInfo.size = fileInfo.Length;

					objHeadData.FileInfosDic.Add (path, fileBlockInfo);


					offset += fileBlockInfo.size;

					using (FileStream fin = new FileStream(path, FileMode.Open))
					{
//						byte[] buf = new byte[fin.Length];
//						fin.Read(buf, 0, buf.Length);
//						bodyStream.Write(fin, 0, buf.Length);

						fin.CopyTo (bodyStream);
					}

				}

				MemoryStream headStream = new MemoryStream ();

				BinaryFormatter b = new BinaryFormatter();
				b.Serialize(headStream, objHeadData);


				byte[] intBytes = LongToBytes(headStream.Length);

				fout.Write (intBytes, 0, intBytes.Length);
				headStream.Position = 0;
				headStream.CopyTo (fout);
				bodyStream.Position = 0;
				bodyStream.CopyTo (fout);

			}

		}

		static byte[] LongToBytes(long num)
		{
			//大小端
			#if BIG_ENDIAN
				num = IPAddress.NetworkToHostOrder(num);
			#endif

			//定长 8byte
			byte[] bytes = BitConverter.GetBytes(num);

			return bytes;
		}

		static List<FileInfo> GetFileInfoList(string rsc)
		{
			List<DirectoryInfo> DirInfoList = new List<DirectoryInfo>();

			DirInfoList.Add(new DirectoryInfo(rsc));
			int DirInfoListIndex = 0;

			while (DirInfoListIndex < DirInfoList.Count)
			{
				DirectoryInfo[] dirs = new DirectoryInfo[] { };
				try
				{
					dirs = DirInfoList[DirInfoListIndex].GetDirectories();

				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}

				DirInfoList.AddRange(dirs);
				DirInfoListIndex++;
			}

			List<FileInfo> fileInfoList = new List<FileInfo> ();
			foreach (DirectoryInfo dirInfo in DirInfoList) 
			{
				fileInfoList.AddRange(dirInfo.GetFiles ());
			}

			return fileInfoList;

		}

	}
}


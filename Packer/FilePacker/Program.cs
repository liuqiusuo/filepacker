﻿using System;
using System.IO;
namespace FilePacker
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string targetPath;
			string resourcePath;

			if (false) 
			{
				if (args.Length < 2) {
					Console.WriteLine ("need 2 arguments!");
					return;
				}
					
				targetPath = args [0];
				resourcePath = args [1];

				if (File.Exists (targetPath)) {
					Console.WriteLine ("target file is existed!");
					return;
				}

				if (!Directory.Exists (resourcePath)) {
					Console.WriteLine ("Pack directory is not existed!");
					return;				
				}
			}

			targetPath = "/Users/admin/Projects/SampleFiles";
			resourcePath = "/Users/admin/Projects/SampleFiles";

			PackCmd.Pack (targetPath, resourcePath);

		}
	}
}
